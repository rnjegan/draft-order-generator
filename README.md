# Order Generator for Drafts (e.g. NFL or other sports drafts) 

## Requirements
- Only Python 3 is required, and only the standard package "random" is used

## Run this Script
- Simply run the sript on the commandline via  
For seven players: `python Order-Generator_seven.py -s [seed]`, with see being a number between 0 and 9
For eight players: `python Order-Generator_eight.py -s [seed]`, with see being a number between 0 and 9

## For Seven Players:
## Draft Setup
- The Script is setup to generate three picks for each player
- Currently we have seven players
- Each players chooses two Win-Teams (i.e. one point for every win by that team) and one Loss-Team (i.e. one point for every loss by that team)
- To fairly distribute the picks, a draft algorithm was needed, due to several factors
  - Teams cannot be chosen more than once within the fourteen Win-Teams and within the seven Loss-Teams  
  --> That means Team A can only be chosen once as a Win-Team, however Team A can be chosen again as a Loss-Team
  - That means, theoretically, the first pick for a Win-Team is as valuable as the first pick for the Loss-Team
- Therefore, the algorithms works as follows:
  - There are fourteen Win-Teams, and the first pick is worth 14 points.
  - The second pick is worth 13 points, the third 12 points, and so on, until the fourteenth pick (last Win-Team), which is worth 1 point.
  - Since the first Loss-Pick is worth as much as the first Win-Team, it is worth 14 points again, the second Loss-Team 13 points, and so on, until the seventh Loss-Pick (twentyfirst pick overall), which is worth 8 points.
  - So we have the following poinst across all picks: 14+13+12+11+10+9+8+7+6+5+4+3+2+1+14+13+12+11+10+9+8=182
  - Since we have seven players, 182/7=26
  - Therefore, we need a draft algorithm, that distributes three picks across seven players, that are worth 26 points for each player.
  - This is achieved via brute-force in the script `Order-Generator.py` and works reasonably efficient. 

## Script Output
- A sample output from the script is:
~~~
{'14: Win Pick 14': 1, '02: Win Pick 02': 13, '17: Loss Pick 03': 12},
{'13: Win Pick 13': 2, '04: Win Pick 04': 11, '16: Loss Pick 02': 13},
{'10: Win Pick 10': 5, '03: Win Pick 03': 12, '20: Loss Pick 06': 9},
{'08: Win Pick 08': 7, '07: Win Pick 07': 8, '18: Loss Pick 04': 11},
{'12: Win Pick 12': 3, '06: Win Pick 06': 9, '15: Loss Pick 01': 14},
{'05: Win Pick 05': 10, '09: Win Pick 09': 6, '19: Loss Pick 05': 10},
{'01: Win Pick 01': 14, '11: Win Pick 11': 4, '21: Loss Pick 07': 8},
'Max retries for this result-order: 54'
~~~

Each line displays the picks for one player.  
Line one `{'14: Win Pick 14': 1, '02: Win Pick 02': 13, '17: Loss Pick 03': 12}` means:
- The picks are not sorted, that means the first pick from this player is printed after the first comma
- The first pick for this player is therefore the second pick, which is worth 13 points (Win-Team)
- The second pick for this player is the fourteenth pick, which is worth 1 point (Win-Team)
- The third pick for this player is the seventeenth pick, which is worth 12 points (Loss-Team)
- These picks add up to 26 points, therefore adhering to the rules described above
The number at the end `54` is the number of iterations the algorithms tried to guess, until a solution for all seven players was found.

## For Eight Players
### Draft Setup
- The Script is setup to generate three picks for each player
- Currently we have eight players
- Each players chooses two Win-Teams (i.e. one point for every win by that team) and one Loss-Team (i.e. one point for every loss by that team)
- To fairly distribute the picks, a draft algorithm was needed, due to several factors
  - Teams cannot be chosen more than once within the sixteen Win-Teams and within the eight Loss-Teams  
  --> That means Team A can only be chosen once as a Win-Team, however Team A can be chosen again as a Loss-Team
  - That means, theoretically, the first pick for a Win-Team is as valuable as the first pick for the Loss-Team
- Therefore, the algorithms works as follows:
  - There are sixteen Win-Teams, and the first pick is worth 16 points.
  - The second pick is worth 15 points, the third 14 points, and so on, until the sixteenth pick (last Win-Team), which is worth 1 point.
  - Since the first Loss-Pick is worth as much as the first Win-Team, it is worth 16 points again, the second Loss-Team 15 points, and so on, until the eigth Loss-Pick (twentyfourth pick overall), which is worth 9 points.
  - So we have the following poinst across all picks: 16+15+14+13+12+11+10+9+8+7+6+5+4+3+2+1+16+15+14+13+12+11+10+9=236
  - Since we have eight players, 236/8=29.5 --> due to decimal digit, 29 and 30 will be accepted for sum of picks' worths
  - Therefore, we need a draft algorithm, that distributes three picks across seven players, that are worth 118 points for each player.
  - This is achieved via brute-force in the script `Order-Generator.py` and works reasonably efficient. 

### Script Output
- A sample output from the script is:
~~~
{'04: Win Pick 04': 13, '13: Win Pick 13': 4, '21: Loss Pick 05': 12}, 
{'15: Win Pick 15': 2, '05: Win Pick 05': 12, '17: Loss Pick 01': 16}, 
{'02: Win Pick 02': 15, '12: Win Pick 12': 5, '24: Loss Pick 07': 9}, 
{'07: Win Pick 07': 10, '08: Win Pick 08': 9, '22: Loss Pick 06': 11}, 
{'06: Win Pick 06': 11, '11: Win Pick 11': 6, '20: Loss Pick 04': 13}, 
{'10: Win Pick 10': 7, '09: Win Pick 09': 8, '19: Loss Pick 03': 14}, 
{'01: Win Pick 01': 16, '14: Win Pick 14': 3, '23: Loss Pick 07': 10}, 
{'16: Win Pick 16': 1, '03: Win Pick 03': 14, '18: Loss Pick 02': 15}, 
'Max retries for this result-order: 47'
~~~

Each line displays the picks for one player.  
Line one `{'04: Win Pick 04': 13, '13: Win Pick 13': 4, '21: Loss Pick 05': 12}` means:
- The picks are not sorted, that means the first pick from this player is printed after the first comma
- The first pick for this player is therefore the fourth pick, which is worth 13 points (Win-Team)
- The second pick for this player is the thirteenth pick, which is worth 4 point (Win-Team)
- The third pick for this player is the twentyfirst pick, which is worth 12 points (Loss-Team)
- These picks add up to 29 points, therefore adhering to the rules described above
The number at the end `47` is the number of iterations the algorithms tried to guess, until a solution for all eight players was found.
