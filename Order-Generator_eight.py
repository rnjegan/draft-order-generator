import random, argparse

def seed_type(x):
    x = int(x)
    if x > 9:
        raise argparse.ArgumentTypeError("Maximum value is 9")
    return x

parser = argparse.ArgumentParser(description='Generate Draft Order')
parser.add_argument("-s", "--seed", type=seed_type, help="target bandwidth <= 9")

args = parser.parse_args()
seed = args.seed
print(f"Seed value: {seed}")

# Weight calculation:
# 16+15+14+13+12+11+10+9+8+7+6+5+4+3+2+1+16+15+14+13+12+11+10+9=236
# 236/8=29.5
pick_weights = 29
max_retries = 100

win_picks = {"01: Win Pick 01" : 16, "02: Win Pick 02" : 15, "03: Win Pick 03" : 14, 
        "04: Win Pick 04" : 13, "05: Win Pick 05" : 12, "06: Win Pick 06" : 11, 
        "07: Win Pick 07" : 10, "08: Win Pick 08" : 9, "09: Win Pick 09" : 8, 
        "10: Win Pick 10" : 7, "11: Win Pick 11" : 6, "12: Win Pick 12" : 5,
        "13: Win Pick 13" : 4, "14: Win Pick 14" : 3, "15: Win Pick 15" : 2,
        "16: Win Pick 16" : 1}
loss_picks = {"17: Loss Pick 01" : 16, "18: Loss Pick 02" : 15, "19: Loss Pick 03" : 14, 
        "20: Loss Pick 04" : 13, "21: Loss Pick 05" : 12, "22: Loss Pick 06" : 11, 
        "23: Loss Pick 07" : 10, "24: Loss Pick 07" : 9}

def get_picks_for_one_player(player_name, remaining_win_picks, remaining_loss_picks, retries):
        player = {}
        chosen_picks = []
        retries += 1

        # add first pick
        first_pick = random.choice(list(remaining_win_picks))
        player[first_pick] = remaining_win_picks[first_pick]
        chosen_picks.append(first_pick)
        reduced_win_picks = {key:value for key, value in remaining_win_picks.items() if key not in chosen_picks}
        # add second pick
        second_pick = random.choice(list(reduced_win_picks))
        player[second_pick] = reduced_win_picks[second_pick]
        chosen_picks.append(second_pick)
        reduced_win_picks = {key:value for key, value in reduced_win_picks.items() if key not in chosen_picks}
        # add third pick
        third_pick = random.choice(list(remaining_loss_picks))
        player[third_pick] = remaining_loss_picks[third_pick]
        chosen_picks.append(third_pick)
        reduced_loss_picks = {key:value for key, value in remaining_loss_picks.items() if key not in chosen_picks}

        player_worth = sum(player.values())

        # continue and allow player_worth being two different sums (otherwise no solution can be found)
        if (player_worth in [pick_weights, pick_weights+1]):
                return player, reduced_win_picks, reduced_loss_picks, retries
        else:
                if retries > max_retries:
                        return False
                else:
                        return get_picks_for_one_player(player_name, remaining_win_picks, remaining_loss_picks, retries)
                

counter = 0
def get_picks():
        global counter
        while True:
                result = get_picks_for_one_player("Player 01", win_picks, loss_picks, 0)
                if result:
                        player_01, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries   
                        continue
                result = get_picks_for_one_player("Player 02", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_02, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 03", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_03, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 04", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_04, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 05", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_05, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 06", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_06, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 07", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_07, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 08", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_08, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                        break
                else: 
                        counter += max_retries 
                        continue
        current_counter = f"Max retries for this result-order: {counter}"
        counter = 0
        return player_01, player_02, player_03, player_04, player_05, player_06, player_07, player_08, current_counter


final_picks = get_picks()
several_results = []
for i in range(10):
        one_result = get_picks()
        several_results.append(one_result)
print(several_results)

print()
print(f"Use result-order for seed {seed}:")
player_list = ["Hellen", "Jessi", "Jonas", "Krissi", "Micha", "Patrick", "Robin", "Tobi"]
final_player_order = {}
seeded_order = list(several_results[seed])
print(seeded_order)
for player in player_list:
    final_player_order[player] = seeded_order.pop(0)
for player in final_player_order:
    player_worth = sum(final_player_order[player].values())
    print(f"player: {player} has the following picks: {final_player_order[player]} with sum: {player_worth}")