import random, argparse

def seed_type(x):
    x = int(x)
    if x > 9:
        raise argparse.ArgumentTypeError("Maximum value is 9")
    return x

parser = argparse.ArgumentParser(description='Generate Draft Order')
parser.add_argument("-s", "--seed", type=seed_type, help="target bandwidth <= 9")

args = parser.parse_args()
seed = args.seed
print(f"Seed value: {seed}")

# Weight calculation:
# 14+13+12+11+10+9+8+7+6+5+4+3+2+1+14+13+12+11+10+9+8=182
# 182/7=26
pick_weights = 26
max_retries = 100

win_picks = {"01: Win Pick 01" : 14, "02: Win Pick 02" : 13, "03: Win Pick 03" : 12, 
        "04: Win Pick 04" : 11, "05: Win Pick 05" : 10, "06: Win Pick 06" : 9, 
        "07: Win Pick 07" : 8, "08: Win Pick 08" : 7, "09: Win Pick 09" : 6, 
        "10: Win Pick 10" : 5, "11: Win Pick 11" : 4, "12: Win Pick 12" : 3,
        "13: Win Pick 13" : 2, "14: Win Pick 14" : 1}
loss_picks = {"15: Loss Pick 01" : 14, "16: Loss Pick 02" : 13, "17: Loss Pick 03" : 12, 
        "18: Loss Pick 04" : 11, "19: Loss Pick 05" : 10, "20: Loss Pick 06" : 9, 
        "21: Loss Pick 07" : 8}

def get_picks_for_one_player(player_name, remaining_win_picks, remaining_loss_picks, retries):
        player = {}
        chosen_picks = []
        retries += 1

        # add first pick
        first_pick = random.choice(list(remaining_win_picks))
        player[first_pick] = remaining_win_picks[first_pick]
        chosen_picks.append(first_pick)
        reduced_win_picks = {key:value for key, value in remaining_win_picks.items() if key not in chosen_picks}
        # add second pick
        second_pick = random.choice(list(reduced_win_picks))
        player[second_pick] = reduced_win_picks[second_pick]
        chosen_picks.append(second_pick)
        reduced_win_picks = {key:value for key, value in reduced_win_picks.items() if key not in chosen_picks}
        # add third pick
        third_pick = random.choice(list(remaining_loss_picks))
        player[third_pick] = remaining_loss_picks[third_pick]
        chosen_picks.append(third_pick)
        reduced_loss_picks = {key:value for key, value in remaining_loss_picks.items() if key not in chosen_picks}

        # continue 
        if (sum(player.values()) != pick_weights):
                if retries > max_retries:
                        return False
                else:
                        return get_picks_for_one_player(player_name, remaining_win_picks, remaining_loss_picks, retries)
        else:
                return player, reduced_win_picks, reduced_loss_picks, retries

counter = 0
def get_picks():
        global counter
        while True:
                result = get_picks_for_one_player("Player 01", win_picks, loss_picks, 0)
                if result:
                        player_01, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries   
                        continue
                result = get_picks_for_one_player("Player 02", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_02, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 03", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_03, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 04", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_04, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 05", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_05, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 06", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_06, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                else: 
                        counter += max_retries 
                        continue
                result = get_picks_for_one_player("Player 07", remaining_win_picks, remaining_loss_picks, 0) 
                if result:
                        player_07, remaining_win_picks, remaining_loss_picks, retries = result
                        counter += retries
                        break
                else: 
                        counter += max_retries 
                        continue
        current_counter = f"Max retries for this result-order: {counter}"
        counter = 0
        return player_01, player_02, player_03, player_04, player_05, player_06, player_07, current_counter


final_picks = get_picks()
several_results = []
for i in range(10):
        one_result = get_picks()
        several_results.append(one_result)
print(several_results)

print()
print(f"Use result-order for seed {seed}:")
player_list = ["Hellen", "Jessi", "Krissi", "Micha", "Patrick", "Robin", "Tobi"]
final_player_order = {}
seeded_order = list(several_results[seed])
print(seeded_order)
for player in player_list:
    final_player_order[player] = seeded_order.pop(0)
for player in final_player_order:
    print(f"player: {player} has the following picks: {final_player_order[player]}")